import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {DataService} from './services/data.service';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {AppComponent} from './app.component';
import {BlackBoxesComponent} from './components/black-boxes/black-boxes.component';
import {ProtocolsComponent} from './components/protocols/protocols.component';
import {PeriodsComponent} from './components/periods/periods.component';
import {SummaryComponent} from './components/summary/summary.component';
import {FormsModule} from '@angular/forms';

@NgModule({
    declarations: [
        AppComponent,
        BlackBoxesComponent,
        ProtocolsComponent,
        PeriodsComponent,
        SummaryComponent
    ],
    imports: [
        BrowserModule, FormsModule, NgbModule.forRoot()
    ],
    providers: [
        DataService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
