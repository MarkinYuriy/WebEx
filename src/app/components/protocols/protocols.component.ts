import {Component, OnInit} from '@angular/core';
import {DataService} from  './../../services/data.service'
@Component({
    selector: 'app-protocols',
    templateUrl: './protocols.component.html',
    styleUrls: ['./protocols.component.css']
})
export class ProtocolsComponent implements OnInit {


    constructor(private  dataService: DataService) {
        this.data = this.dataService.getData().protocols;
        this.selectedProtocols = this.dataService.getSelectedProtocols();

    }

    ngOnInit() {
    }

    data = {};
    selectedProtocols = {}
    filter_parent = []

    onFilterChange(event: any, ind) {
        this.dataService.setSelectedProtocols(
            {key: this.data[ind].id, value: this.data[ind].name},
            event
        )
    }
}
