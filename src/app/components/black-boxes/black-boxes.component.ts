import {Component, OnInit} from '@angular/core';
import {DataService} from './../../services/data.service'

@Component({
    selector: 'app-black-boxes',
    templateUrl: './black-boxes.component.html',
    styleUrls: ['./black-boxes.component.css']
})
export class BlackBoxesComponent implements OnInit {

    constructor(private  dataService: DataService) {
        this.data = this.dataService.getData().device_groups;
        this.data.forEach((deviceGroup, i) => {
            this.filter_parent[i] = false;
            this.filter_childs[i] = []
            deviceGroup.devices.forEach((device, j) => {

                if (device.active == 1) {
                    this.filter_childs[i][j] = true;
                } else {
                    this.filter_childs[i][j] = false;
                }
            })
            this.filter_parent[i] = this.checkIsAllSelectedInGroup(i)
        })

    }

    ngOnInit() {

    }

    data: any = []
    isCollapsed = [];
    filter_parent: any = [];
    filter_childs: any = [];

    onFilterChange(event: any, isParent, indChild, indParent) {
        if (isParent) {
            this.data[indParent].devices.forEach((device, j) => {
                this.filter_parent[indParent] = event
                this.filter_childs[indParent][j] = event;
                if (event) {
                    device.active = 1;
                } else {
                    device.active = 0;
                }
            })
        } else {
            this.filter_childs[indParent][indChild] = event;
            if (event) {
                this.data[indParent].devices[indChild].active = 1;
            } else {
                this.data[indParent].devices[indChild].active = 0;
            }
            this.filter_parent[indParent] = this.checkIsAllSelectedInGroup(indParent)
        }


    }

    checkIsAllSelectedInGroup(indParent) {
        let counter = 0;
        this.data[indParent].devices.forEach((device) => {
            counter += device.active;
        })
        if (counter >= this.data[indParent].devices.length) {
            return true;
        } else {
            return false;
        }
    }
}
