import {Component, OnInit} from '@angular/core';
import {DataService} from './../../services/data.service'


@Component({
    selector: 'app-summary',
    templateUrl: './summary.component.html',
    styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
    constructor(private  dataService: DataService) {
        this.data = this.dataService.getData();
        this.selectedProtocols = this.dataService.getSelectedProtocols()
        this.selectedPeriod = this.dataService.selectedPeriod
    }

    ngOnInit() {
    }

    data = {};
    selectedProtocols = {};
    selectedPeriod = {}
    objectKeys = Object.keys;

    onClear() {
        this.dataService.onClear()
    }
}
