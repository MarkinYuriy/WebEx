import {Component, OnInit} from '@angular/core';
import {DataService} from  './../../services/data.service'

@Component({
    selector: 'app-periods',
    templateUrl: './periods.component.html',
    styleUrls: ['./periods.component.css']
})
export class PeriodsComponent implements OnInit {
    constructor(private  dataService: DataService) {
        this.data = this.dataService.getData().times;
        this.dataService.setSelectedPeriod({key: this.data[0].id, value: this.data[0].name})
        this.selectedPeriod = this.dataService.getSelectedPeriod()

    }

    ngOnInit() {
    }

    data = {};
    selectedPeriod = {}

    onFilterChange(ind) {
        this.dataService.setSelectedPeriod({key: this.data[ind].id, value: this.data[ind].name})
    }
}

